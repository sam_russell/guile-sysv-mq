CFLAGS = `guile-config compile` -fPIC
LIBS = `guile-config link`

KEY_FILE = "dammit_jim_im_a_doctor_not_a_system_call"

.PHONY: clean build run

build: guile_sysv_mq

clean:
	rm -f guile_sysv_mq guile_sysv_mq.o

run: guile_sysv_mq
	./guile_sysv_mq

guile_sysv_mq: guile_sysv_mq.o
	gcc $< -o $@ $(LIBS)
	touch $(KEY_FILE)

guile_sysv_mq.o: guile_sysv_mq.c
	gcc -c $< -o $@ $(CFLAGS)


