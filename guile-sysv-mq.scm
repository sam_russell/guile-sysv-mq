;;; (c) Sam Russell
;;; GPLv3
;;; pizzapal@sdf.org

(define mq-lib (dynamic-link "./guile-sysv-mq.so"))

(dynamic-call "init_sysv_mq" mq-lib)

(define (get-msg) (recv-message (start-queue) 1))
(define (send-msg s) (send-message (start-queue) 1 s))



