/*
 __________________
/ (c) Sam Russell  \
| GPLv3            |
\ pizzapal@sdf.org /
 ------------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#include <libguile.h>


typedef struct msgbuf
{
  long mtype;
  char mtext[500];
} msgbuf;

int start_queue (key_t k)
{
  return msgget (k, IPC_CREAT | 0660);
}

static SCM s_start_queue ()
{
  key_t key;
  key = ftok ("dammit_jim_im_a_doctor_not_a_system_call",
	      3);
  int id;
  id = start_queue (key);
  SCM result = scm_from_int (id);
  return result;
}


int post (int id, msgbuf m)
{
  return msgsnd(id, &m, sizeof(m), 0);
}

SCM s_post_msg (SCM id, SCM msg_t, SCM msg_text)
{
  char* s;
  s = scm_to_locale_string (msg_text);
  long t = scm_to_long (msg_t);
  struct msgbuf m = {t};
  strcpy (m.mtext, s);
  
  int mq_id;
  mq_id = scm_to_int (id);
  int sent;
  sent = post (mq_id, m);
  return scm_from_int (sent);
}

int recv (int id, msgbuf* buf, long t)
{
  return msgrcv(id, buf, sizeof(msgbuf), t, IPC_NOWAIT);
}

SCM s_recv_msg (SCM id, SCM t)
{
  msgbuf msgdata;
  long msg_t;
  msg_t = scm_to_long (t);
  int mq_id;
  mq_id = scm_to_int (id);
  int status;
  status = recv(mq_id, &msgdata, msg_t);
  if (status == -1)
    {
      /*
      return scm_from_int (status);
      */
      return scm_from_utf8_string ( strerror (errno) );
    }
  else
    {
      char* s;
      s = msgdata.mtext;
      return scm_from_utf8_string (s);
      /*      return scm_from_locale_string (s); */
    }
}



static void* register_functions (void* data)
{
  scm_c_define_gsubr ("start-queue", 0, 0, 0, &s_start_queue);
  scm_c_define_gsubr ("send-message", 3, 0, 0, &s_post_msg);
  scm_c_define_gsubr ("recv-message", 2, 0, 0, &s_recv_msg);
  return NULL;
}

void init_sysv_mq ()
{
  scm_c_define_gsubr ("start-queue", 0, 0, 0, &s_start_queue);
  scm_c_define_gsubr ("send-message", 3, 0, 0, &s_post_msg);
  scm_c_define_gsubr ("recv-message", 2, 0, 0, &s_recv_msg);
}


int main (int argc, char* argv[])
{
  scm_with_guile (&register_functions, NULL);
  scm_shell (argc, argv);
}
